package com.uchanxinchan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UchanXinchanApplication {

	public static void main(String[] args) {
		SpringApplication.run(UchanXinchanApplication.class, args);
	}

}
